const { expect } = require("chai");
const { ethers } = require("hardhat");
const mintNftAbi = require("../../web/src/config/artifacts/contracts/facets/MintNFTFacet.sol/MintNFTFacet.json");
// const Diamond = artifacts.require("Diamond");

describe("Contract", function() {
      let diamondAddress;

      describe("MintNFT", function() {
        it("Should create NFTs", async function() {
          const DiamondCutFacet = await ethers.getContractFactory("DiamondCutFacet")
          const diamondCutFacet = await DiamondCutFacet.deploy();
          await diamondCutFacet.deployed();

          const Diamond = await ethers.getContractFactory("Diamond")
          const diamond = await Diamond.deploy("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266",diamondCutFacet.address);
          await diamond.deployed();

          const MintNFTFacet = await ethers.getContractFactory("MintNFTFacet")
          const mintNFTFacet = await MintNFTFacet.deploy();
          await mintNFTFacet.deployed();
          console.log("MinNftAddress: ", mintNFTFacet.address);
          console.log("DiamondAddress: ",diamond.address);

          diamondAddress = diamond.address;

          const provider = new ethers.providers.JsonRpcProvider("http://localhost:8545");
          const signer =  provider.getSigner();
          const MintNFTContract = new ethers.Contract(diamondAddress, mintNftAbi.abi, signer);
          const nft = await MintNFTContract.createToken("https://www.mytoken1location.com");
          const number = await MintNFTContract.randomNumber();

          console.log(await nft.wait())

        })
        // it("Random NFT", async function() {
        //   const provider = new ethers.providers.JsonRpcProvider("http://localhost:8545");
        //   const signer =  provider.getSigner();
        //   console.log(await signer.getAddress());
        //   console.log('diamond', await provider.getCode(await signer.getAddress()));
        //   const MintNFTContract = new ethers.Contract(diamondAddress, mintNftAbi.abi, provider);
        //   const rand = await MintNFTContract.randomNumber();
        //   console.log('rand', rand);
        // })
      })

  })

// describe("Diamond contract", function() {
//   describe("Deployment", function() {
//     it("Contract should be deployed", async function() {
//       const Diamond = await Diamond.new("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266", "0x5FbDB2315678afecb367f032d93F642f64180aa3");
//       // await Diamond.createToken("https://www.mytoken1location.com");
//     })
//   })
// })


