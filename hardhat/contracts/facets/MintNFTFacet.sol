// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import {Item, AppStorage, LibAppStorage} from '../libraries/LibAppStorage.sol';

contract MintNFTFacet is ERC721URIStorage, Ownable {

    constructor() ERC721("sneakers","SNK") {}

    function createToken(string memory tokenURI) public returns(uint256) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        uint256 itemId = randomNumber();
        _safeMint(msg.sender, itemId);
        _setTokenURI(itemId, tokenURI);
        Item memory item = Item(itemId, address(this));
        s.items.push(item);
        return itemId;
    }

    function getItemId(uint index) public view returns(uint ) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        return s.items[index].itemId;
    }

    function getItems() public view returns(Item[] memory) {
        AppStorage storage s = LibAppStorage.diamondStorage();
        return s.items;
    }

    function randomNumber() public view returns(uint){
        return uint(keccak256(abi.encodePacked(block.timestamp, block.difficulty, msg.sender))) % 1000000;
    }
}
