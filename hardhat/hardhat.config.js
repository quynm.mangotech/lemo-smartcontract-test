require('dotenv').config()
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require('@nomiclabs/hardhat-truffle5')
require('hardhat-contract-sizer');
require('@nomiclabs/hardhat-waffle')

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task('accounts', 'Prints the list of accounts', async () => {
  const accounts = await ethers.getSigners()
  for (const account of accounts) {
    console.log(account.address)
  }
})

module.exports = {
  contractSizer: {
      alphaSort: true,
      disambiguatePaths: false,
      runOnCompile: true,
      strict: true,
  },
  solidity: {
    version: "0.8.10",
    optimizer: {
      enabled: true,
      runs: 200
    }
  },
  paths: {
    artifacts: "../web/src/config/artifacts"
  },
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
      chainId: 1337,
      mining: {
        auto: true,
        //auto: false,
        //interval: 1618
      },
      // docker: {
      //   url:'http://ganache-cli:8545',
      //   chainId: 1337,
      //   mining: {
      //     auto: true,
      //   }
      // },
      // accounts: {
      //   mnemonic: process.env.MNEMONIC
      // },
      allowUnlimitedContractSize: false,
    },

  },
};
